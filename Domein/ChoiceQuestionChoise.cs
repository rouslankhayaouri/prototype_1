﻿namespace Domein
{
    class ChoiceQuestionChoise
    {
        public int Id { get; set; }
        public ChoiseQuestion Question { get; set; }
        public  ChoiseOption Choise { get; set; }
    }
}
