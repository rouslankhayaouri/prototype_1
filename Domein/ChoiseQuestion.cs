﻿using System.Collections.Generic;

namespace Domein
{
    public class ChoiseQuestion
    {
        public int Id { get; set; }
        public string ChoiseText { get; set; }
        public ChoiseOption Option1 { get; set; }
        public ChoiseOption Option2 { get; set; }
        public List<Keyword> choiseMatchCollection { get; set; }
        public Policy Policy { get; set; }

        public ChoiseQuestion()
        {
            choiseMatchCollection = new List<Keyword>();
        }
    }
}
