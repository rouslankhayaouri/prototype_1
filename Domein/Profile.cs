﻿using System.Collections.Generic;

namespace Domein
{
    class Profile
    {
        public int Id { get; set; }
        public List<PersonaQuestionChoise> PersonaProfile { get; set; }
        public List<ChoiceQuestionChoise> ChoicesProfile { get; set; }
        public City LocationCity { get; set; }

        public Profile()
        {
            PersonaProfile = new List<PersonaQuestionChoise>();
            ChoicesProfile = new List<ChoiceQuestionChoise>();
        }

    }
}
