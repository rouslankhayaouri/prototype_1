﻿using System.Collections.Generic;

namespace Domein
{
    public class ChoiseOption
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public List<Property> Properties { get; set; }
        public Keyword OptionKeyword { get; set; }

        public ChoiseOption()
        {
            Properties = new List<Property>();
        }
    }
}
