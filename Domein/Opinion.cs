﻿namespace Domein
{
    class Opinion
    {
        public int Id { get; set; }
        public string OpinionText { get; set; }
        public Party OpinionOfParty { get; set; }
        public Policy OpinionOnPolicy { get; set; }
    }
}
