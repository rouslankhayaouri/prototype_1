﻿namespace Domein
{
    public class Property
    {
        public enum Type
        {
            Good, Bad
        }
        public Type QualityType { get; set; }
        public string Text { get; set; }
    }
}
