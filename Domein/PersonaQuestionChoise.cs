﻿namespace Domein
{
    class PersonaQuestionChoise
    {
        public int Id { get; set; }
        public PersonaQuestion Question { get; set; }
        public PersonaOption Choise { get; set; }
    }
}
