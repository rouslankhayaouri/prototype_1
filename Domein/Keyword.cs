﻿namespace Domein
{
    public class Keyword
    {
        public int Id { get; set; }
        public string KeywordText { get; set; }
    }

}