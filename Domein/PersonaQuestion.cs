﻿namespace Domein
{
    public class PersonaQuestion
    {
        public string QuestionText { get; set; }
        public PersonaOption Option1 { get; set; }
        public PersonaOption Option2 { get; set; }
        public int Id { get; set; }
    }
}
