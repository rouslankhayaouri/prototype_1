﻿namespace Domein
{
    public class PersonaOption
    {
        public string OptionText { get; set; }
        public Keyword OptionKeyword { get; set; }
    }
}
