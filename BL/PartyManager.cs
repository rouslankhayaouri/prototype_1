﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Domein;

namespace BL
{
    public class PartyManager
    {
        private PartyRepository repository;

        public PartyManager()
        {
            repository = PartyRepository.Instance;
        }

        public Party GetParty(int id)
        {
            return repository.ReadParty(id);
        }

        public List<Party> GetAllParties()
        {
            return repository.ReadAllParties();
        }

        public void AddParty(Party party)
        {
            repository.CreateParty(party);
        }

        public void ChangeParty(Party party)
        {
            repository.UpdateParty(party);
        }

        public void RemoveParty(int id)
        {
            repository.DeleteParty(id);
        }
    }
}
