﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using Domein;

namespace BL
{
    public class CityManager
    {
        private CityRepository repository= CityRepository.Instance;

        public void AddCity(City city)
        {
            int index = repository.GetAlCities().Max(c => c.Id);
            city.Id = index+1;
            repository.CreateCity(city);
        }

        public City GetCity(int id)
        {
            return repository.ReadCity(id);
        }

        public void ChangeCity(City city)
        {
            repository.UpdateCity(city);
        }

        public List<City> GetCities()
        {
            return repository.GetAlCities();
        }

        public void RemoveCity(City city)
        {
            repository.Delete(city);
        }

        public void RemoveCity(int id)
        {
            City city = repository.ReadCity(id);
            repository.Delete(city);
        }
    }
}
