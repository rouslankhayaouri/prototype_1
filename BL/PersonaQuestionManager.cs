﻿using System.Collections.Generic;
using DAL;
using Domein;

namespace BL
{
    public class PersonaQuestionManager
    {
        private PersonaQuestionRepository repository = PersonaQuestionRepository.Instance;

        public void AddPersonaQuestion(PersonaQuestion question)
        {
            repository.CreatePersonaQuestion(question);
        }

        public PersonaQuestion GetPersonaQuestion(int id)
        {
            return repository.ReadPersonaQuestion(id);
        }

        public IReadOnlyCollection<PersonaQuestion> GetAllPersonaQuestions()
        {
            return repository.ReadAllPersonaQuestions();
        }

        public void ChangePersonaQuestion(PersonaQuestion question)
        {
            repository.UpdatePersonaQuestion(question);
        }

        public void RemovePersonaQuestion(int id)
        {
            repository.DeletePersonaQuestion(id);
        }
    }
}
