﻿using System.Collections.Generic;
using DAL;
using Domein;

namespace BL
{
    public class KeywordManager
    {
        private KeywordRepository repository = KeywordRepository.Instance;

        public void AddKeyword(string keyword)
        {
            repository.CreateKeyword(keyword);
        }

        public Keyword GetKeyword(int id)
        {
            return repository.ReadKeyword(id);
        }

        public IReadOnlyCollection<Keyword> GetAllKeywords()
        {
            return repository.ReadKeywords();
        }

        public void ChangeKeyword(Keyword keyword)
        {
            repository.UpdateKeyword(keyword);
        }

        public void RemoveKeyword(int id)
        {
            repository.DeleteKeyword(id);
        }
    }
}
