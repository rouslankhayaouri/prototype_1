﻿using System.Collections.ObjectModel;
using DAL;
using Domein;

namespace BL
{
    public class ChoisequestionManager
    {
        private ChoiseQuestionRepository repository = ChoiseQuestionRepository.Instance;

        public void AddChoiseQuestion(ChoiseQuestion question)
        {
            repository.CreateChoiseQuestion(question);
        }

        public ChoiseQuestion GetChoiseQuestion(int id)
        {
            return repository.ReadChoiseQuestion(id);
        }

        public ReadOnlyCollection<ChoiseQuestion> GetAllChoiseQuestions()
        {
            return repository.ReadAllChoiseQuestions();
        }

        public void ChangeChoiseQuestion(ChoiseQuestion question)
        {
            repository.UpdateChoiseQuestion(question);
        }

        public void RemoveChoiseQuestion(int id)
        {
            repository.DeleteChoiseQuestion(id);
        }
    }
}
