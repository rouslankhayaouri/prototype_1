﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Domein;

namespace DAL
{
    public class PersonaQuestionRepository
    {
        private List<PersonaQuestion> questions;
        private static PersonaQuestionRepository instance;
        private static int PQId = 1;

        private PersonaQuestionRepository()
        {
            questions = new List<PersonaQuestion>();
        }

        public static PersonaQuestionRepository Instance
        {
            get
            {
                if(instance == null)
                    instance = new PersonaQuestionRepository();
                return instance;
            }
        }

        public void CreatePersonaQuestion(PersonaQuestion question)
        {
            question.Id = PQId;
            questions.Add(question);
            PQId++;
        }

        public PersonaQuestion ReadPersonaQuestion(int id)
        {
            return questions.Find(q => q.Id == id);
        }

        public IReadOnlyCollection<PersonaQuestion> ReadAllPersonaQuestions()
        {
            return new ReadOnlyCollection<PersonaQuestion>(questions);
        }

        public void UpdatePersonaQuestion(PersonaQuestion question)
        {
            // all in memory
        }

        public void DeletePersonaQuestion(int id)
        {
            PersonaQuestion question = ReadPersonaQuestion(id);
            questions.Remove(question);
        }
    }
}
