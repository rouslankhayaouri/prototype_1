﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Domein;

namespace DAL
{
    public class KeywordRepository
    {
        private List<Keyword> keywords;
        private static int IdCounter = 1;
        private static KeywordRepository instance;

        private KeywordRepository()
        {
            keywords = new List<Keyword>();
            seed();
        }

        public static KeywordRepository Instance
        {
            get
            {
                if(instance == null)
                    instance = new KeywordRepository();
                return instance;
            }
        }

        public void CreateKeyword(string keyword)
        {
            keywords.Add(new Keyword(){KeywordText = keyword, Id = IdCounter});
            IdCounter = IdCounter + 1;
        }

        public Keyword ReadKeyword(int id)
        {
            return keywords.Find(k => k.Id == id);
        }

        public ReadOnlyCollection<Keyword> ReadKeywords()
        {
            return new ReadOnlyCollection<Keyword>(keywords);
        }

        public void UpdateKeyword(Keyword keyword)
        {
            //all in memory
        }

        public void DeleteKeyword(int id)
        {
            Keyword keyword = ReadKeyword(id);
            keywords.Remove(keyword);
        }

        private void seed()
        {
            string[] keywordStrings = new[] {"man", "vrouw", "Onder 30", "Boven 30", "heeft kinderen", "heeft geen kinderen"};
            foreach (string keywordString in keywordStrings)
            {
                CreateKeyword(keywordString);
            }
        }
    }
}
