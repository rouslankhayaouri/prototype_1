﻿using System.Collections.Generic;
using Domein;

namespace DAL
{
    public class PartyRepository
    {
        private List<Party> parties;
        private static PartyRepository _instance;

        private PartyRepository()
        {
            parties = new List<Party>();
            seed();
        }

        public static PartyRepository Instance
        {
            get
            {
                if(_instance == null)
                    _instance = new PartyRepository();
                return _instance;
            }
        }

        public List<Party> ReadAllParties()
        {
            return new List<Party>(parties);
        }
        public Party ReadParty(int id)
        {
            return parties.Find(p => p.Id == id);
        }

        public void UpdateParty(Party party)
        {
            //all in memory
        }

        public void CreateParty(Party party)
        {
            parties.Add(party);
        }

        public void DeleteParty(int id)
        {
            Party party = ReadParty(id);
            parties.Remove(party);
        }

        private void seed()
        {
            CityRepository repository = CityRepository.Instance;
            parties.Add(new Party(){Id = 1, Name = "NVA", LoctionCity = repository.ReadCity(1)});
            parties.Add(new Party() { Id = 2, Name = "OpenVLD", LoctionCity = repository.ReadCity(3) });
            parties.Add(new Party() { Id = 3, Name = "VB", LoctionCity = repository.ReadCity(1) });
        }
    }
}
