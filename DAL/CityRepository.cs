﻿using System.Collections.Generic;
using Domein;

namespace DAL
{
    public class CityRepository
    {
        private static CityRepository instance;
        private List<City> cities;

        private CityRepository()
        {
            cities = new List<City>();
            seed();
        }

        public static CityRepository Instance
        {
            get
            {
                if(instance == null)
                    instance = new CityRepository();
                return instance;
            }
        }

        public void CreateCity(City city)
        {
            cities.Add(city);
        }

        public City ReadCity(int id)
        {
            return cities.Find(c => c.Id == id);
        }

        public void UpdateCity(City city)
        {
            // not needed
        }

        public void Delete(City city)
        {
            cities.Remove(city);
        }

        public List<City> GetAlCities()
        {
            return new List<City>(cities);
        }

        private void seed()
        {
            cities.Add(new City(){Id = 1, Name = "Brussel", Zipcode = 1000});
            cities.Add(new City(){Id = 2, Name = "Laken", Zipcode = 1020});
            cities.Add(new City(){Id = 3, Name = "Schaarbeek", Zipcode = 1030});
            cities.Add(new City(){Id = 4, Name = "Etterbeek", Zipcode = 1040});
            cities.Add(new City(){Id = 5, Name = "Elsene", Zipcode = 1050});
        }
    }
}
