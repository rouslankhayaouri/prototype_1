﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Domein;

namespace DAL
{
    public class ChoiseQuestionRepository
    {
        private static ChoiseQuestionRepository instance;
        private static List<ChoiseQuestion> questions;
        private static int IdCounter = 1;

        private ChoiseQuestionRepository()
        {
            questions = new List<ChoiseQuestion>();
        }

        public static ChoiseQuestionRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ChoiseQuestionRepository();
                }
                return instance;
            }
        }

        public void CreateChoiseQuestion(ChoiseQuestion question)
        {
            question.Id = IdCounter;
            questions.Add(question);
            IdCounter++;
        }

        public ReadOnlyCollection<ChoiseQuestion> ReadAllChoiseQuestions()
        {
            return new ReadOnlyCollection<ChoiseQuestion>(questions);
        }

        public ChoiseQuestion ReadChoiseQuestion(int id)
        {
            return questions.Find(q => q.Id == id);
        }

        public void UpdateChoiseQuestion(ChoiseQuestion question)
        {
            // all in memory
        }

        public void DeleteChoiseQuestion(int id)
        {
            ChoiseQuestion question = ReadChoiseQuestion(id);
            questions.Remove(question);
        }
    }
}
