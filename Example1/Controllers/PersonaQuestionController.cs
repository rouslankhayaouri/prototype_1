﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL;
using Domein;

namespace Example1.Controllers
{
    public class PersonaQuestionController : Controller
    {
        private PersonaQuestionManager PQManager = new PersonaQuestionManager();
        private KeywordManager KManager = new KeywordManager();
        // GET: PersonaQuestion
        public ActionResult Index()
        {
            return View(PQManager.GetAllPersonaQuestions());
        }

        // GET: PersonaQuestion/Details/5
        public ActionResult Details(int id)
        {
            return View(PQManager.GetPersonaQuestion(id));
        }

        // GET: PersonaQuestion/Create
        public ActionResult Create()
        {
            return View(KManager.GetAllKeywords());
        }

        // POST: PersonaQuestion/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {

                PersonaOption option1 = new PersonaOption()
                {
                    OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keyword1"])),
                    OptionText = Convert.ToString(collection["option1Text"])
                };
                PersonaOption option2 = new PersonaOption()
                {
                    OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keyword2"])),
                    OptionText = Convert.ToString(collection["option2Text"])
                };
                PersonaQuestion question = new PersonaQuestion()
                {
                    QuestionText = Convert.ToString(collection["question"]),
                    Option1 = option1,
                    Option2 = option2
                };
                PQManager.AddPersonaQuestion(question);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PersonaQuestion/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Question = PQManager.GetPersonaQuestion(id);
            ViewBag.Keywords = KManager.GetAllKeywords();
            return View();
        }

        // POST: PersonaQuestion/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                int Id = Convert.ToInt32(collection["Id"]);
                PersonaQuestion question = PQManager.GetPersonaQuestion(Id);
                question.Option1.OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keyword1"]));
                question.Option1.OptionText = Convert.ToString(collection["option1Text"]);
                question.QuestionText = Convert.ToString(collection["question"]);
                question.Option2.OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keyword2"]));
                question.Option2.OptionText = Convert.ToString(collection["option2Text"]);
                PQManager.ChangePersonaQuestion(question);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PersonaQuestion/Delete/5
        public ActionResult Delete(int id)
        {
            return View(PQManager.GetPersonaQuestion(id));
        }

        // POST: PersonaQuestion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                PQManager.RemovePersonaQuestion(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
