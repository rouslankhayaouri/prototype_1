﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL;
using Domein;

namespace Example1.Controllers
{
    public class CityController : Controller
    {
        private CityManager manager = new CityManager();
        // GET: City
        public ActionResult Index()
        {
            List<City> cities = manager.GetCities();
            return View(cities.ToList());
        }

        // GET: City/Details/5
        public ActionResult Details(int id)
        {
            City city = manager.GetCity(id);
            return View(city);
        }

        // GET: City/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: City/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                City city = new City()
                {
                    Name = Convert.ToString(collection["name"]),
                    Zipcode = Convert.ToInt32(collection["zipcode"])
                };
                manager.AddCity(city);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: City/Edit/5
        public ActionResult Edit(int id)
        {
            City city = manager.GetCity(id);
            return View(city);
        }

        // POST: City/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                City city = manager.GetCity(Convert.ToInt32(collection["id"]));
                city.Name = Convert.ToString(collection["name"]);
                city.Zipcode = Convert.ToInt32(collection["zipcode"]);
                manager.ChangeCity(city);
               // manager.ChangeCity(city);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: City/Delete/5
        public ActionResult Delete(int id)
        {
            City city = manager.GetCity(id);
            return View(city);
        }

        // POST: City/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                manager.RemoveCity(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
