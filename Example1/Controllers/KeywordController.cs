﻿using System;
using System.Web.Mvc;
using BL;
using Domein;

namespace Example1.Controllers
{
    public class KeywordController : Controller
    {
        private KeywordManager manager = new KeywordManager();
        // GET: Keyword
        public ActionResult Index()
        {
            return View(manager.GetAllKeywords());
        }

        // GET: Keyword/Details/5
        public ActionResult Details(int id)
        {
            return View(manager.GetKeyword(id));
        }

        // GET: Keyword/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Keyword/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                manager.AddKeyword(Convert.ToString(collection["keyword"]));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Keyword/Edit/5
        public ActionResult Edit(int id)
        {
            return View(manager.GetKeyword(id));
        }

        // POST: Keyword/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Keyword keyword = manager.GetKeyword(id);
                keyword.KeywordText = Convert.ToString(collection["keyword"]);
                manager.ChangeKeyword(keyword);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Keyword/Delete/5
        public ActionResult Delete(int id)
        {
            return View(manager.GetKeyword(id));
        }

        // POST: Keyword/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                manager.RemoveKeyword(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
