﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL;
using Domein;

namespace Example1.Controllers
{
    public class PartyController : Controller
    {
        private PartyManager manager = new PartyManager();
        private CityManager cManager = new CityManager();
        // GET: Party
        public ActionResult Index()
        {
            return View(manager.GetAllParties());
        }

        // GET: Party/Details/5
        public ActionResult Details(int id)
        {
            return View(manager.GetParty(id));
        }

        // GET: Party/Create
        public ActionResult Create()
        {
            List<City> cities = cManager.GetCities();
            return View(cities);
        }

        // POST: Party/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            //http://localhost:50279/Party/Create?id=7&naam=Groen&city=2
            try
            {
                // TODO: Add insert logic here
                City city = cManager.GetCity(Convert.ToInt32(collection["city"]));


                Party party = new Party()
                {
                    Id = Convert.ToInt32(collection["id"]),
                    Name = Convert.ToString(collection["naam"]),
                    LoctionCity = city
                };

                manager.AddParty(party);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Oeps()
        {
            return View();
        }

        // GET: Party/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Cities = cManager.GetCities();
            ViewBag.Party = manager.GetParty(id);
            return View();
        }

        // POST: Party/Edit/5
        [HttpPost]
        public ActionResult Edit(int id,FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                City city = cManager.GetCity(Convert.ToInt32(collection["city"]));
                int pId = Convert.ToInt32(collection["id"]);
                Party party = manager.GetParty(pId);
                party.Name = Convert.ToString(collection["naam"]);
                party.LoctionCity = city;
                manager.ChangeParty(party);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Party/Delete/5
        public ActionResult Delete(int id)
        {
            return View(manager.GetParty(id));
        }

        // POST: Party/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                manager.RemoveParty(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
