﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BL;
using Domein;

namespace Example1.Controllers
{
    public class ChoiseQuestionController : Controller
    {
        private KeywordManager KManager = new KeywordManager();
        private ChoisequestionManager CQManager = new ChoisequestionManager();
        // GET: ChoiseQuestion
        public ActionResult Index()
        {
            return View(CQManager.GetAllChoiseQuestions());
        }

        // GET: ChoiseQuestion/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.ChoiseQuestion = CQManager.GetChoiseQuestion(id);
            return View();
        }

        // GET: ChoiseQuestion/Create
        public ActionResult Create()
        {
            //vraag me niet waarom
            ViewBag.KeywordsOptie1 = KManager.GetAllKeywords();
            ViewBag.KeywordsOptie2 = KManager.GetAllKeywords();
            ViewBag.Choise = KManager.GetAllKeywords();
            return View();
        }

        // POST: ChoiseQuestion/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Property propertyOption1 = new Property()
                {
                    QualityType = Property.Type.Good,
                    Text = Convert.ToString(collection["eigenschapOptie1"])
                };
                ChoiseOption option1 = new ChoiseOption()
                {
                    Text = Convert.ToString(collection["option1Text"]),
                    //Properties = new List<Property>(),
                    OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keywordOptie1"]))
                };
                option1.Properties.Add(propertyOption1);

                Property propertyOption2 = new Property()
                {
                    QualityType = Property.Type.Good,
                    Text = Convert.ToString(collection["eigenschapOptie2"])
                };
                ChoiseOption option2 = new ChoiseOption()
                {
                    Text = Convert.ToString(collection["option2Text"]),
                    //OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keywordOptie2"])),
                    //Properties = new List<Property>(),
                    OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keywordOptie2"]))
                };
                option2.Properties.Add(propertyOption2);

                //Keyword questionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keywordMatch"]));
                ChoiseQuestion question = new ChoiseQuestion()
                {
                    //choiseMatchCollection = new List<Keyword>(),
                    ChoiseText = Convert.ToString(collection["question"]),
                    Option1 = option1,
                    Option2 = option2
                };
                //question.choiseMatchCollection.Add(questionKeyword);
                question.choiseMatchCollection.Add(KManager.GetKeyword(Convert.ToInt32(collection["keywordMatch"])));
                CQManager.AddChoiseQuestion(question);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ChoiseQuestion/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            @ViewBag.Keywords = KManager.GetAllKeywords();
            @ViewBag.ChoiseQuestion = CQManager.GetChoiseQuestion(id);
            return View();
        }

        // POST: ChoiseQuestion/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
   
            try
            {
                // TODO: Add update logic here
                ChoiseQuestion question = CQManager.GetChoiseQuestion(Convert.ToInt32(collection["questionId"]));

                question.Option1.Properties[0].Text = Convert.ToString(collection["eigenschapOptie1"]);
                question.Option1.Text = Convert.ToString(collection["option1Text"]);
                question.Option1.OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keywordOptie1"]));

                question.Option2.Properties[0].Text = Convert.ToString(collection["eigenschapOptie2"]);
                question.Option2.Text = Convert.ToString(collection["option2Text"]);
                question.Option2.OptionKeyword = KManager.GetKeyword(Convert.ToInt32(collection["keywordOptie2"]));

                question.ChoiseText = Convert.ToString(collection["question"]);
                question.choiseMatchCollection[0] = KManager.GetKeyword(Convert.ToInt32(collection["keywordMatch"]));

                CQManager.ChangeChoiseQuestion(question);



                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ChoiseQuestion/Delete/5
        public ActionResult Delete(int id)
        {
            return View(CQManager.GetChoiseQuestion(id));
        }

        // POST: ChoiseQuestion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                CQManager.RemoveChoiseQuestion(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ChoiseQuestion/AddProperty
        public ActionResult AddProperty()
        {
            return View();
        }

        // POST: ChoiseQuestion/AddProperty
        [HttpPost]
        public ActionResult AddProperty(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
